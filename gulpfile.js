const gulp = require('gulp');

const sass = require('gulp-sass');

const browserSync = require('browser-sync').create();

gulp.task('sass', function () {
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream({match: '**/*.css'}));
});

function watch() {
    browserSync.init({
        server: {
            baseDir: "app"
        }
    });
    gulp.watch('app/scss/**/*.scss', gulp.parallel('sass'));
    gulp.watch("app/*.html").on('change', browserSync.reload);
}

gulp.task('watch', watch);